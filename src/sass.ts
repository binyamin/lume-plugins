import { Page, path, type Site } from './deps.ts';

import {
	compileString,
	StringOptions,
} from 'https://denopkg.dev/gh/binyamin/deno-sass@main/mod.ts';

type Options = Omit<StringOptions<'sync'>, 'url' | 'syntax'>;

const SassPlugin = (options: Options = {}) => {
	return function (site: Site) {
		site.loadAssets(['.scss', '.sass']);
		site.process(['.scss', '.sass'], (file) => {
			if (path.basename(file.src.path).startsWith('_')) {
				return;
			}

			const output = compileString(file.content as string, {
				...options,
				url: path.toFileUrl(site.src(file.src.path + file.src.ext)),
				syntax: file.src.ext === '.sass' ? 'indented' : 'scss',
			});

			file.updateDest({
				ext: '.css',
			});

			file.content = output.css;

			if (output.sourceMap) {
				file.content += `\n/*# sourceMappingURL=${
					path.basename(file.dest.path + file.dest.ext + '.map')
				} */`;

				const mapFile = Page.create(
					file.dest.path + file.dest.ext + '.map',
					JSON.stringify(output.sourceMap),
				);

				site.pages.push(mapFile);
			}
		});
	};
};

export default SassPlugin;
