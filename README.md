# Lume Plugins

Various plugins for [Lume](https://lume.land), a static-site generator for
[Deno](https://deno.land).

## Usage

Import the module from GitLab, using https://denopkg.dev:

```js
import {
	clean_css,
	sass,
} from 'https://denopkg.dev/gl/binyamin/lume-plugins@main/mod.ts';
```

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/lume-plugins/-/blob/main/LICENSE).
Copyright 2022 Binyamin Aron Green.
